from src.Locally_Linear_Embedding import LocallyLinearEmbedding as LocLinEmb
from src.plot_data import Plot3D, Plot2D
import torch
from sklearn.datasets import make_swiss_roll


def main():
    x, y = make_swiss_roll(1000, noise=0.05)  # torch.load('data/swiss_roll_data.pt')
    test = LocLinEmb(features=x, n_neighbours=20, n_components=2)
    trans = test()
    #Plot3D(x, y, plot_name='Sample data')
    Plot2D(torch.real(trans), y, plot_name='Make it stop')


if __name__ == "__main__":
    main()
