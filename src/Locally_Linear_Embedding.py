import pandas as pd
import torch
import sklearn.manifold


# from sklearn.manifold import LocallyLinearEmbedding  # for LLE dimensionality reduction
# from sklearn.manifold import Isomap  # for Isomap dimensionality reduction


class LocallyLinearEmbedding:
    def __init__(self, features, n_neighbours: int, n_components: int):
        """
        Constructor for class LocallyLinearEmbedding
        :param features: Sample data
        :param n_neighbours: Number of neighbours considered for each point
        :param n_components: Dimension of target space
        """
        self.features = torch.tensor(features)
        self.n_samples = self.features.shape[0]
        self.n_im = self.features.shape[1]
        self.n_neighbours = n_neighbours
        self.n_components = n_components

    def __call__(self):
        W = self.get_weight_matrix()
        # Compute the Laplacian of W
        temp = torch.eye(n=self.n_samples) - W
        laplacian = temp.T @ temp
        # Do the eigenvalue decomposition
        eig_val, eig_vec = torch.linalg.eigh(laplacian)
        idx = torch.abs(eig_val).argsort()
        # Select the eigenvectors corresponding to the smallest eigenvalues
        Y = eig_vec[:, idx[1:self.n_components + 1]]
        return Y

    def get_neighbours(self):
        # Calculate the pairwise distance matrix
        dist = torch.cdist(self.features, self.features)
        # Select the index of the k points closest to each point, excluding the points themselves
        indices = dist.topk(k=self.n_neighbours + 1, largest=False)[1][:, 1:]
        return indices

    def get_weight_matrix(self):
        k = self.n_neighbours
        n = self.n_samples
        features = self.features
        # Calculate the pairwise distance matrix
        dist = torch.cdist(features, features)
        # Select the index of the k points closest to each point, excluding the points themselves
        neighbour_indices = dist.topk(k=k + 1, largest=False)[1][:, 1:]

        # Now we need to calculate the Gram matrices corresponding to each point
        gram = torch.zeros(n, k, k)
        ones = torch.ones(k, 1)
        for sample_idx, point in enumerate(features):
            neighbours = features[neighbour_indices[sample_idx]]
            G_idx = ones * point - neighbours
            gram[sample_idx, :, :] = G_idx @ G_idx.transpose(0, 1) + 1E-5 * torch.eye(k)
        # With the Gram matrices at our disposal, we calculate the weights
        inv = gram.inverse()
        W_ = (inv @ ones / (ones.transpose(0, 1) @ inv @ ones)).mT
        # The only thing left is to plug in the non-zero values into our weight matrix
        W = torch.zeros(n, n)
        for idx in range(n):
            W[idx][neighbour_indices[idx]] = W_[idx]
        return W

    @staticmethod
    def safe_inverse(x: torch.tensor):
        try:
            inv = x.inverse()
        except Exception as ex:
            print(f'Warning: {ex}')
            inv = x.pinverse()
        return inv
